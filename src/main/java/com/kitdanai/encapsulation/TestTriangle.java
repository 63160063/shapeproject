/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.encapsulation;

/**
 *
 * @author ADMIN
 */
public class TestTriangle {
    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(5,8);
        System.out.println("Area of triangle1(H = "+ triangle1.getH()+" B = "+triangle1.getB()+") is "+triangle1.calAreaTri());
        triangle1.setH_B(2, 6);
        System.out.println("Area of triangle1(H = "+ triangle1.getH()+" B = "+triangle1.getB()+") is "+triangle1.calAreaTri());
        triangle1.setH_B(0, 0);
        System.out.println("Area of triangle1(H = "+ triangle1.getH()+" B = "+triangle1.getB()+") is "+triangle1.calAreaTri());
        
    }
}
