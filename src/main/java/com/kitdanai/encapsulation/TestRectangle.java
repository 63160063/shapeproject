/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.encapsulation;

/**
 *
 * @author ADMIN
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(3,5);
        System.out.println("Area of rectangle1(W = "+ rectangle1.getW()+" L = "+rectangle1.getL()+") is "+rectangle1.calAreaRec());
        rectangle1.setW_L(4, 8);
        System.out.println("Area of rectangle1(W = "+ rectangle1.getW()+" L = "+rectangle1.getL()+") is "+rectangle1.calAreaRec());
        rectangle1.setW_L(0, 0);
        System.out.println("Area of rectangle1(W = "+ rectangle1.getW()+" L = "+rectangle1.getL()+") is "+rectangle1.calAreaRec());
        
    }
}
