/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.encapsulation;

/**
 *
 * @author ADMIN
 */
public class Triangle {
    private double H,B ;
    public static final double cons = 1/2 ;
    public Triangle (double H,double B){
        this.H = H;
        this.B = B;
    }
    public double calAreaTri (){
        return cons * H * B ;
    }
    public double getH(){
        return H;
    }
    public double getB(){
        return B;
    }
    public void setH_B(double H,double B){
        if(H <=0 && B <= 0){
            System.out.println("Error : Heigh and Base must more than zero !!!");
            return;
        }
        this.H = H;
        this.B = B;
    }
}
