/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.encapsulation;

/**
 *
 * @author ADMIN
 */
public class Rectangle {
    private double W,L ;
    public Rectangle(double W ,double L){
        this.W = W;
        this.L = L;
    }
    public double calAreaRec(){
        return W * L;
    }
    public double getW(){
        return W;
    }
     public double getL(){
        return L;
    }
     public void setW_L (double W,double L){
         if(W <= 0 && L <= 0){
             System.out.println("Error : Width and Long must more than zero !!");
             return ;
         }
         this.L = L;
         this.W = W;
     }
}
