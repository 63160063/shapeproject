/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.encapsulation;

/**
 *
 * @author ADMIN
 */
public class Square {
     private double S ;
    public Square(double S){
        this.S = S;
    }
    public double calAreaSquare(){
        return S * S;
    }
    public double getS(){
        return S;
    }
     public void setS(double S){
         if(S <= 0){
             System.out.println("Error : Side must more than zero !!");
             return ;
         }
         this.S = S;
}
}
