/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kitdanai.encapsulation;

/**
 *
 * @author ADMIN
 */
public class TestSquare {
    public static void main(String[] args) {
        Square square1 = new Square(5);
        System.out.println("Area of Square(S = "+ square1.getS()+") is "+square1.calAreaSquare());
        square1.setS(9);
        System.out.println("Area of Square(S = "+ square1.getS()+") is "+square1.calAreaSquare());
        square1.setS(0);
        System.out.println("Area of Square(S = "+ square1.getS()+") is "+square1.calAreaSquare());
    }
}
